class Solution:
    def decodeString(self, s: str) -> str:
        stack = []
        for c in s:
            if c != ']':
                stack.append(c)
                continue

            acc = ""
            while stack[-1] != "[":
                top = stack.pop()
                acc = top + acc
            stack.pop() #delete [ from the stack

            num_acc = ""
            while len(stack) > 0 and stack[-1].isdigit():
                top = stack.pop()
                num_acc = top + num_acc

            stack.append(acc * int(num_acc))

        return ''.join(stack)


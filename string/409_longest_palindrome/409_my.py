class Solution:
    def longestPalindrome(self, s: str) -> int:
        d = {}
        odd = False
        res = 0
        for c in s:
            try:
                d[c] += 1
            except:
                d[c] = 1

        for k, v in d.items():
            if v % 2 == 0:
                res += v
            elif not odd:
                odd = True
                res += v
            else:
                res += (v-1)

        return res

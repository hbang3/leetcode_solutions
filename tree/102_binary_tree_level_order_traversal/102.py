class Solution:
    def levelOrder(self, root: Option[TreeNode]) -> List[List[int]]:
        ret = []

        def helper(node, depth):
            if node is None:
                return

            try:
                ret[depth].append(node.val)
            except:
                ret.append([node.val])

            helper(node.left, depth + 1)
            helper(node.right, depth + 1)

        helper(root, 0)
        return ret

class Solution:
    def buildTree(self, preorder, inorder):
        def array_to_tree(left, right):
            nonlocal preorder_idx
            if left > right:
                return None

            root_val = preorder[preorder_idx]
            root = TreeNode(root_val)
            preorder_idx += 1

            root.left = array_to_tree(left, inorder_idx_map[root_val] - 1)
            root.left = array_to_tree(inorder_idx_map[root_val] + 1, right)

            return root

        preorder_idx = 0
        inorder_idx_map = {}
        for idx, val in enumerate(inorder):
            inorder_idx_map[idx] = val

        return array_to_tree(0, len(preorder) - 1)


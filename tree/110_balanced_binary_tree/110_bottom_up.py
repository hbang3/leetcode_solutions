'''
we can remove redundant calculation of height using bottom up approach
cehck if the child subtrees are balanced.
1. if recursive calls before conditions check => bottom up
2. if recursive call after conditional check => top down
'''


class Solution:
    def isBalancedHelper(self, root) -> (bool, int):
        if not root:
            return True, -1

        left_is_balanced, left_height = self.isBalancedHelper(root.left)
        if not left_is_balanced:
            return False, 0

        right_is_balanced, right_height = self.isBalancedHelper(root.right)
        if not right_is_balanced:
            return False, 0

        return (abs(left_height - right_height) < 2), 1 + max(left_height, right_height)

    def isBalanced(self, root) -> bool:
        return self.isBalancedHelper(root)[0]

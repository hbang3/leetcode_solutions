class Solution:
    def height(self, node):
        if not node:
            return 0
        return 1 + max(self.height(node.left), self.height(node.right))

    def isBalanced(self, root) -> bool:
        if not root:
            return True
        return (abs(self.height(root.left) - self.height(root.right))) < 2 \
            and self.isBalanced(root.left) \
            and self.isBalanced(root.right)

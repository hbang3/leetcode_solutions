class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        def helper(node, level):
            if node is None:
                return
            else:
                if level in ret:
                    ret[level].append(node.val)
                else:
                    ret[level] = [node.val]

            helper(node.right, level + 1) 
            helper(node.left, level + 1) 
        ret = dict()
        helper(root, 0)
        ans = []
        for key, val in ret.items():
            ans.append(val[0])
        return ans

def Solution:
    def longestZigZag(self, root):
        def dfs(node, path, depth):
            nonlocal max_dep
            if node is None:
                return

            max_dep = max(max_dep, depth)
            if path == 'l' and node.left:
                dfs(node.left, 'l', 1)
            if path == 'l' and node.right:
                dfs(node.right, 'r', depth+1)
            if path == 'r' and node.left:
                dfs(node.left, 'l', depth+1)
            if path == 'r' and node.right:
                dfs(node.right, 'r', 1) 

        max_len = 0
        dfs(root.left, 'l', 1)
        dfs(root.right, 'r', 1)

        return max_len

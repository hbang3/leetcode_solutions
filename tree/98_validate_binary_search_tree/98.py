class Solution:
    def isValidBST(self, root: Optional[TreeNode]):
        def helper(node, floor, ceil):
            if node is None:
                return True

            if node.val <= floor or node.val >= ceil:
                return False

            return helper(node.left, floor, node.val) and helper(node.right, node.val, ceil)

        return helper(root, -math.inf, math.inf) 

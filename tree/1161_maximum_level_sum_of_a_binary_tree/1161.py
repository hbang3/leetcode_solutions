def Solution:
    def maxLevelSum(self, root) -> int:
        ans, level, sum_at_level, max_sum = 0,0,0, float('-inf')
        queue = collections.deque([root])

        while queue:
            level += 1
            sum_at_level = 0
            for _ in range(len(queue)):
                node = queue.popleft()
                sum_at_level += node.val

                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)

            if sum_at_level > max_sum:
                max_sum, ans = sum_at_level, level

        return ans

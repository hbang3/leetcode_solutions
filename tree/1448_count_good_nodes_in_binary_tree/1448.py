class Solution:
    def goodNodes(self, root) -> int:
        def dfs(node, max_val):
            nonlocal cnt
            if not node:
                return
            
            if node.val >= max_val:
                cnt += 1
                max_val = node.val

            if node.left: 
                dfs(node.left, max_val)
            if node.right:
                dfs(node.right, max_val) 

        cnt = 0
        dfs(root, root.val)
        return cnt

    def goodNodes2(self, root) -> int:
        cnt = 0
        queue = collections.deque([(root, root.val)])
        while queue:
            node,max_val = queue.popleft()
            if max_val <= node.val:
                cnt += 1
            if node.right:
                queue.append((node.right, max(node.val, max_val)))
            if node.left:
                queue.append((node.left , max(node.val, max_val)))

        return cnt

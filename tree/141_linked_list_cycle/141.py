class Solution:
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        if not head or not head.next:
            return False

        sp = head
        fp = head.next
        while fp:
            for _ in range(2):
                if sp == fp:
                    return True
                if fp.next:
                    fp = fp.next
                else:
                    return False
            sp = sp.next

        return False

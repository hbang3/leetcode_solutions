class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        '''
        1. p < node < q -> return node
        2. node < p < q -> check right
        3. p < q < node -> check left
        4. node == p -> p
        5. node == q -> q
        '''

        if p.val > q.val:
            p, q = q, p

        def dfs(node):
            if node == p or node == q:
                return node

            if node.val < p.val:
                return dfs(node.right)
            if node.val > q.val:
                return dfs(node.left)
            if p.val < node.val < q.val:
                return node

        return dfs(root)

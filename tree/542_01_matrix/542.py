class Solution:
    def updateMatrix(self, mat: List[List[int]]) -> List[List[int]]:
        def valid(r, c):
            return 0 <= r < len(mat) and 0 <= c < len(mat[0])

        ret = [row[:] for row in mat]
        queue = collections.deque()
        seen = set()

        for r in range(len(mat)):
            for c in range(len(mat[0])):
                if ret[r][c] == 0:
                    queue.append((r, c, 0))
                    seen.add((r, c))

        dir = [
            (0, 1),
            (1, 0),
            (0, -1),
            (-1, 0),
        ]

        while queue:
            r, c, steps = queue.popleft()
            for dr, dc in dir:
                nr, nc = r + dr, c + dc
                if (nr, nc) not in seen and valid(nr, nc):
                    seen.add((nr, nc))
                    queue.append((nr, nc, steps + 1))
                    ret[nr, nc] = steps + 1

        return ret

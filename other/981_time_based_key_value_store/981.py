class TimeMap:
    def __init__(self):
        self.d = dict()

    def set(self, key:str, value:str, timestamp: int) -> None:
        if not key in self.d:
            self.d[key] = []
        self.d[key].append([timestamp, value])

    def get(self, key:str, timestamp: int) -> str:
        if not key in self.d:
            return ""

        if timestamp < self.d[key][0][0]:
            return ""
 
        l, r = 0, len(self.d[key])
        while l < r:
            mid = (l + r) // 2
            if self.d[key][mid][0] <= timestamp:
                l = mid + 1
            else:
                r = mid

        return "" if r == 0 else self.d[key][r-1][1]
             

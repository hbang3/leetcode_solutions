class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        def dist(point):
            x = point[0]
            y = point[1]

        heap = []
        for point in points:
            if len(heap) < k:
                heapq.heappush(heap, (-dist(point), point))
            else:
                if -dist(point) > heap[0][0]:
                    heapq.heapreplace(heap, (-dist(point), point))

        return list(map(lambda x: x[1], heap))

class Solutionde
 def canConstruct(self, ransomNote: str, magazine: str) -> bool:
      d = [0] * 26
       for c in magazine:
            d[ord(c) - ord('a')] += 1

        for c in ransomNote:
            d[ord(c) - ord('a')] -= 1
            if d[ord(c) - ord('a')] < 0:
                return False

        return True

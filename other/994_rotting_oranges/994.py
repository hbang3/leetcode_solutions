class Solution:
    def orangeRotting(self, grid):
        def isValid(r,c):
            return 0 <= r < len(grid) and 0 <= c < len(grid[0])
        
        queue = collections.deque([])
        fresh = set()
        ans = 0

        for r in range(len(grid)):
            for c in range(len(grid[0])):
                if grid[r][c] == 2:
                    queue.append((r,c,0))
                if grid[r][c] == 1:
                    fresh.add((r,c))

        dir = [(0,1), (1,0), (0, -1), (-1,0)]
        ans = -1 if len(fresh) != 0 else 0

        while queue:
            r, c, day = queue.popleft()
            ans = max(ans, day)
            for dr, dc in dir:
                nr = r + dr
                nc = c + dc
                if isValid(nr, nc) and (nr, nc) in fresh:
                    queue.append((nr, nc, day+1))
                    fresh.remove((nr, nc))

        if len(fresh) != 0:
            return -1
        
        return ans

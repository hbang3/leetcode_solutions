class Solution:
    def twoSumClosest(self, i, nums, target):
        l = i+1
        r = len(nums) - 1
        while l < r:
            sum = nums[i] + nums[l] + nums[r]
            diff = abs(target-sum) 
            if diff < self.diff:
                self.diff = abs(target - sum)
                self.ans = sum
            if sum == target:
                break
            if sum < target:
                l += 1
            elif sum > target:
                r -= 1

    def threeSumClosest(self, nums: List[int], target: int) -> int:
        self.diff = math.inf
        self.ans = math.inf
        nums.sort()
        for i in range(len(nums)):
            self.twoSumClosest(i, nums, target)
        
        return self.ans

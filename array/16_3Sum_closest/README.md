### 16 ###
Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

Return the sum of the three integers.

You may assume that each input would have exactly one solution.

#### Intuition #####
this is similiar problem to number 15, 3Sum. <br>
the only differences is the new variable diff. 

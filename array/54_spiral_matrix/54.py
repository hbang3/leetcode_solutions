class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        directions = [(0,1), (1,0), (0, -1), (-1,0)]
        dir_idx = 0
        visited = set()
        ret = []
        r, c = 0, 0

        def isValid(r, c):
            return 0 <= r < len(matrix) and 0 <= c < len(matrix[0])

        while len(visited) < len(matrix) * len(matrix[0]):
            ret.append(matrix[r][c])
            n_r = r + directions[dir_idx][0]
            n_c = c + directions[dir_idx][1]
            if not isValid(n_r, n_c) or ((n_r, n_c) in visited):  
                dir_idx = (dir_idx + 1) % 4
                r = r + directions[dir_idx][0]
                c = c + directions[dir_idx][1]
            else:
                r = n_r
                c = n_c

        return ret

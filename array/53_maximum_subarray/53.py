class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        '''
        every element - 2 scenarios
        1. add this element
        2. start from this element
        '''
        # iterative way
        dp = [0] * len(nums)
        dp[0] = nums[0]
        for i in range(1, len(nums)):
            dp[i] = max(dp[i-1] + nums[i], nums[i])

        return max(dp)

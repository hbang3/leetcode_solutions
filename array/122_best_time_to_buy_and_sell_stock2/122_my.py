class Solution:
    def maxProfit(self, prices):
        ret = 0
        profits = []
        for i in range(1, len(prices)):
            profits.append(prices[i] - prices[i-1])

        ans = 0
        acc = 0
        for p in profits:
            if p < 0:
                ans += acc
                acc = 0
            else:
                acc += p
        ans += acc

        return ans

class Solution:
    def maxProfit(self, prices):
        acc = 0
        ans = 0
        for i in range(1, len(prices)):
            profit = prices[i] - prices[i-1]
            if profit < 0:
                ans += acc
                acc = 0
            else:
                acc += profit
        ans += acc

        return ans 



class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        def idx(c):
            return ord(c) - ord('a')
        if len(p) > len(s):
            return []

        anag = [0] * 26
        res = [0] * 26
        for i in range(len(p)):
            anag[idx(p[i])] += 1
            res[idx(s[i])] += 1

        ans = [0] if anag == res else []
        for i in range(1, len(s) - len(p) + 1):
            res[idx(s[i-1])] -= 1
            res[idx(s[i + len(p) - 1])] += 1
            if anag == res:
                ans.append(i)

        return ans

class Solution:
    def canCompleteCircuit(self, gas, cost):
        diff = []
        for i in range(len(gas)):
            diff.append(gas[i] - cost[i])

        if sum(diff) < 0:
            return -1

        acc = 0
        ans = 0
        for i in range(len(diff)):
            acc += diff[i]
            if acc < 0:
                acc = 0
                ans = i + 1

        return ans

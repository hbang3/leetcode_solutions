class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, color: int) -> List[List[int]]:
        modified = set()
        stack = []
        modified.add((sr, sc))
        stack.append((sr, sc))
        target_color = image[sr][sc]
        dir = [(0, 1), (1, 0), (-1, 0), (0, -1)]
        while stack:
            r, c = stack.pop(-1)
            for dr, dc in dir:
                nr = r + dr
                nc = c + dc
                if nr < 0 or nc < 0 or nr >= len(image) or nc >= len(image[0]):
                    continue
                if (nr, nc) in modified:
                    continue
                elif image[nr][nc] == target_color:
                    modified.add((nr, nc))
                    stack.append((nr, nc))

        for r, c in modified:
            image[r][c] = color

        return image

module ListQueue = struct
  (* 
     front = [a;b]; back = [e;d;c]
     represents the queue a, b, c, d, e
     if front is empty, back must also be empty
  *)
  type 'a queue = {
    front: 'a list; 
    back: 'a list; 
  }

  let empty = {
    front = [];
    back = [];
  }

  let peek = function
    | {front=[]; back=[]} -> None
    | {front = x :: _} -> Some x
  
  let enqueue x = function
    | {front = []} -> {front = [x]; back = []}
    | q -> {q with back = x :: q.back}

  let dequeue = function
    | {front = []} -> None
    | {front = _ :: []; back} -> Some {front = List.rev back; back=[]}
    | {front = _ :: t; back} -> Some {front = t; back}

end

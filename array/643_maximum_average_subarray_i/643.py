class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        acc = sum(nums[:k])
        curr = acc
        for i in range(k, len(nums)):
            curr = curr - nums[i-k] + nums[i]
            acc = max(acc, curr)

        return acc / k

class Solution:
    def paren_checker(self, o, c):
        c1 = o == "(" and c == ")"
        c2 = o == "[" and c == "]"
        c3 = o == "{" and c == "}"
        return c1 or c2 or c3

    def isValid(self, s: str) -> bool:
        stack = []
        for c in s:
            if c == "(" or c == "[" or c == "{":
                stack.append(c)
                continue
            elif len(stack) == 0:
                return False
            else:
                o = stack.pop(-1)
                if not self.paren_checker(o, c):
                    return False

        return len(stack) == 0

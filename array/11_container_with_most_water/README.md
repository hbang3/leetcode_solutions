### 11 ###
Container with Most Water <br>
You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).

Find two lines that together with the x-axis form a container, such that the container contains the most water.

Return the maximum amount of water a container can store.

Notice that you may not slant the container.

#### Intuition ####
Classic two pointer solutions. <br>
0. initialize l and r pointer to 0, and len(height) - 1 <br>
1. loop<br>
2.      calculate area<br>
3.      ret = max(area, ret)
4.      decide which pointer to move
5. return ret



### 18 ###
4Sum
Given an array nums of n integers, return an array of all the unique quadruplets [nums[a], nums[b], nums[c], nums[d]] such that:

0 <= a, b, c, d < n
a, b, c, and d are distinct.
nums[a] + nums[b] + nums[c] + nums[d] == target
You may return the answer in any order. 

#### Intuition ####
[i, ... j, j+1.......len(nums) - 1) ]
1. pivot i and j
2. call twoSums on nums[j+1:]

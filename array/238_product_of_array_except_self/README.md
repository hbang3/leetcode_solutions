### 238
Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

You must write an algorithm that runs in O(n) time and without using the division operation.

### Intuition
Type of two pointers problem 

[1, 2, 3, 4]
1. calculate prefix 
[1, 1, 2, 6]

2. calculate postfix
[1, 1, 24, 6]
[1, 12, 8, 6]
[24, 12, 8, 6]

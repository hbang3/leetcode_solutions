class Solution:
    def productExceptSelf(self, nums):
        ret = [1] * len(nums)

        forward = 1
        for i in range(len(nums)):
            ret[i] = ret[i] * forward
            forward = forward * nums[i]

        rev = 1
        for i in range(len(nums)-1, -1, -1):
            ret[i] = ret[i] * forward
            rev = rev * nums[i]

        return ret

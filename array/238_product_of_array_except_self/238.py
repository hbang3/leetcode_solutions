class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        prefix, postfix = 1, 1
        ret = [1] * len(nums)

        for i in range(len(nums)):
            if i == 0:
                prefix *= nums[i]
                continue
            ret[i] = prefix
            prefix *= nums[i]

        nums.reverse()
        for j in range(len(nums)):
            if j == 0:
                postfix *= nums[j]
                continue
            ret[len(nums) - j - 1] *= postfix
            postfix *= nums[j]
        return re

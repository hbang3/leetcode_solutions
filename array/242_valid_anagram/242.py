class Solution:
    def isAragram(self, s: str, t: str) -> bool:
        d = [0] * 26
        for c in s:
            d[ord(c) - ord('a')] += 1

        for c in t
        d[ord(c) - ord('a')] -= 1

        for v in d:
            if v != 0:
                return False

        return True

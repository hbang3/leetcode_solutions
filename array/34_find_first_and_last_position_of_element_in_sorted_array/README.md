### 34 ###
Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

If target is not found in the array, return [-1, -1].

You must write an algorithm with O(log n) runtime complexity.

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
Example 3:

Input: nums = [], target = 0
Output: [-1,-1]


#### INTUITION ####
Using binary search two times to find the lowest bound and the highest bound

for lower bound, if nums[mid] == target, we have to check whether it is a lowest bound.
if mid == 0, the lowest bound of target is 0
else if nums[mid - 1] != target, the lowest bound of target is mid
else r = mid -1 and continue binary search


Similary, for upper bound, if nums[mid] == target, we have to check whether it is a highest bound.
if mid == len(nums) - 1, the lowest bound of target is len(nums) - 1 
else if nums[mid + 1] != target, the lowest bound of target is mid
else l = mid + 1 and continue binary search

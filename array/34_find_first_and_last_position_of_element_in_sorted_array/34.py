class Solution:
    def low_bound(self, nums, target):
        l = 0
        r = len(nums) - 1
        while l <= r:
            mid = (l + r) // 2
            if nums[mid] == target:
                if mid == 0:
                    return 0
                elif nums[mid-1] != target:
                    return mid
                else:
                    r = mid - 1
            elif nums[mid] < target:
                l = mid + 1
            elif nums[mid] > target:
                r = mid - 1
        return -1

    def high_bound(self, nums, target):
        l = 0
        r = len(nums) - 1
        while l <= r:
            mid = (l + r) // 2
            if nums[mid] == target:
                if mid == len(nums)-1:
                    return len(nums) - 1 
                elif nums[mid+1] != target:
                    return mid
                else:
                    l = mid + 1
            elif nums[mid] < target:
                l = mid + 1
            elif nums[mid] > target:
                r = mid - 1

        return -1

    def searchRange(self, nums: List[int], target: int) -> List[int]:
        if len(nums) == 0:
            return [-1, -1]
        if target > nums[-1] or target < nums[0]:
            return [-1, -1]
        return [self.low_bound(nums, target), self.high_bound(nums, target)]

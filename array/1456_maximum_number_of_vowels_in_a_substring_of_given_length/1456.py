class Solution:
    def maxVowels(self, s:str, k:int) -> int:
        vowels = set(['a', 'e','i','o','u'])
        cnt = 0
        for c in s[:k]:
            if c in vowels:
                cnt += 1

        max_cnt = cnt
        for i in range(k, len(s)):
            if s[i-k] in vowels:
                cnt -= 1
            if s[i] in vowels:
                cnt += 1

            max_cnt = max(max_cnt, cnt)

        return max_cnt

class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s

        matrix = [[''] * len(s) for _ in range(numRows)]
        dir = [(1, 0), (-1, 1)]
        r, c = 0, 0
        dir_idx = 0
        for i in range(len(s)):
            matrix[r][c] = s[i]
            new_r = r + dir[dir_idx][0]
            new_c = c + dir[dir_idx][1]
            if (new_r >= numRows or new_r < 0):
                dir_idx = (dir_idx + 1) % 2
                new_r = r + dir[dir_idx][0]
                new_c = c + dir[dir_idx][1]
            r = new_r
            c = new_c

        ret = ""
        for m in matrix:
            ret += ''.join(m)

        return ret

### 6, zigzag conversion 

The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);


Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"
Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I
Example 3:

Input: s = "A", numRows = 1
Output: "A"


#### INTUITION
we create two dimensional array
there are two directions that we can populate matrix, (1,0) and (-1, 1)
[                             ]
[                             ]
[                             ]
[                             ]
...
[                             ] 

Given that the string input is "abcdefghijklm....", we populate matrix
[a                             ]
[b                             ]
[c                             ] 
[d                             ]

if we the row number reaches the numRow, we change direction from (1,0) tp (-1, 1)
[a    g                        ]
[b  f                          ]
[c e                           ] 
[d                             ]

if we the row number reaches 0, we change direction from (-1,1) tp (1, 0)
[a    g                        ]
[b  f h                        ]
[c e  i                        ] 
[d    j                        ]

at the end we join each row and add it to return string

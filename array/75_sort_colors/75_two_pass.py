class Solution:
    def sortColors(self, nums: List[int]) -> None:
        zero_cnt = 0
        one_cnt = 0

        for n in nums:
            if n == 0:
                zero_cnt += 1 
            elif n == 1:
                one_cnt += 1  

        for i in range(len(nums)):
            if i < zero_cnt:
                nums[i] = 0
            elif zero_cnt <= i < (zero_cnt + one_cnt):
                nums[i] = 1
            else:
                nums[i] = 2

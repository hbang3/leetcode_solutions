### 15 ###
3Sum <br>
Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.

#### Intuition #####
1. sort the array in non-descending order
2. set nums[i] as pivot and find l, r in nums[i+1:] where nums[l] + nums[r] = -nums[i]

#### some tricks ####
1. if nums[i] > 0: no need to check further
2. it is important to move pointer such as i and l to the point where nums[i] != nums[i-1]

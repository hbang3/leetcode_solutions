class Solution:
    def twoSum (self, i, nums, target):
        l = i+1
        r = len(nums) - 1
        while l < r:
            if nums[l] + nums[r] < target:
                l += 1
            elif nums[l] + nums[r] > target:
                r -= 1
            else: #nums[l] + nums[r] == target
                self.res.append([nums[i], nums[l], nums[r]])
                l += 1
                r -= 1 
                while l < r and nums[l] == nums[l-1]:
                    l += 1

    def threeSum(self, nums: List[int]) -> List[List[int]]:
        self.res = []
        nums.sort()
        for i in range(len(nums)):
            if nums[i] > 0:
                break
            if i == 0 or nums[i-1] != nums[i]:
                target = -nums[i]
                self.twoSum(i, nums, target)
        
        return self.res

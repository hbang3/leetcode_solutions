### 35
Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You must write an algorithm with O(log n) runtime complexity.
#### Intuition
The key of this problem is to use binary search to find the correct position to input target.<br>
notes that if the target is not in the array, the 'left' variable will hold the target position.

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        ins_idx = 1
        n = nums[0]
        for i in range(1, len(nums)):
            if n == nums[i]:
                continue
            else:
                nums[ins_idx] = nums[i]
                n = nums[i]
                ins_idx += 1

        return ins_idx

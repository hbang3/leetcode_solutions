### 26 ###
Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. The relative order of the elements should be kept the same. Then return the number of unique elements in nums.

Consider the number of unique elements of nums to be k, to get accepted, you need to do the following things:

Change the array nums such that the first k elements of nums contain the unique elements in the order they were present in nums initially. The remaining elements of nums are not important as well as the size of nums.
Return k.

#### INTUITION ####
[1, 1, 2, 3, 3, 3, 4, 4, 5]
n = 1
ins_idx = 1
    i
       i

[1, 2, 2, 3, 3, 3, 4, 4, 5]
n = 2
ins_idx = 2
          i
             i

[1, 2, 3, 3, 3, 3, 4, 4, 5]
n = 3
inx_idx = 3
             i
                i 
                   i

[1, 2, 3, 4, 3, 3, 4, 4, 5]
n = 4
ins_idx = 4        
                      i
                          i

[1, 2, 3, 4, 3, 3, 4, 4, 5]
n = 5
ins_idx = 5
return ins_idx


1. n = nums[0] 
2. ins_idx = 1;
3. loop i = 1; i < len(nums); i++:
4.      increment i until you see number not equal to 'n'
        if nums[i] != n
            nums[ins_idx] = nums[i]
            n = nums[i]
            ins_idx += 1

    

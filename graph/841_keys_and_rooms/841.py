class Solution:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:
        visited = set([0])
        stack = [0]

        while stack:
            room_number = stack.pop()
            visited.add(room_number)
            keys = room[room_number]
            for key in keys:
                if key in visited:
                    continue
                stack.append(key)

        return len(visited) == len(rooms)

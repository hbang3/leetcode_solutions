from collections import deque


class Solution:
    def cloneGraph(self, node: Optional['Node']) -> Optional['Node']:
        if not node:
            return node

        visited = {}
        visited[node] = Node(node.val, [])
        queue = deque([node])
        while queue:
            n = queue.popleft()
            for neighbor in n.neighbors:
                if neighbor not in visied:
                    visited[neighbor] = Node(neighbor.val, [])
                    queue.append(neighbor)
                visited[n].neighbors.append(visited[neighbor])

        return visited[node]

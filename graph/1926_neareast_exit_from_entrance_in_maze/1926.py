class Solution:
    def nearestExit(self, maze: List[List[str]], entrance: List[int]) -> int:
        def isValid(r,c):
            return 0 <= r < len(maze) and 0 <= c < len(maze[0])

        [r,c] = entrance
        visited = set([(r,c)])
        queue = collections.deque([(r,c,0)])
        directions = [
            (-1,0),
            (0,-1),
            (1,0),
            (0,1),
        ]

        while queue:
            r, c, d = queue.popleft()
            for dr, dc in directions:
                nr = r + dr
                nc = c + dc
                if (nr, nc) is vistied:
                    continue
                if isValid(nr, nc) and maze[nr][nc] == "+":
                    continue
                if isValid(nr, nc) and maze[nr][nc] == ".":
                    visited.add((nr,nc))
                    queue.append((nr, nc, d+1))
                if [r,c] != entrance and (not isValid(nr,nc)):
                    return d

        return -1

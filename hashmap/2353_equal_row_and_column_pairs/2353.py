class Solution:
    def equalPairs(self, grid) -> int:
        rd = dict()
        cd = dict()

        for i in range(len(grid)):
            rd[i] = grid[i]
            cd[i] = []
            for j in range(len(grid[i])):
                cd[i].append(grid[j][i])

        res = 0
        for key, val in rd.items():
            for i in range(len(val)):
                if val == cd[i]:
                    res += 1
        
        return res
        

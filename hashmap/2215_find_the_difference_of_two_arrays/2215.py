class Solution:
    def findDifference(self, nums1, nums2):
        s1 = set(nums1)
        s2 = set(nums2)

        for n in nums2:
            if n in s1:
                s1.remove(n)

        for n in nums1:
            for n in s2:
                s2.remove(n)

        return [list(s1), list(s2)]

        #one liner
        # return [list(set(nums1) - set(nums2)], list(set(nums2) - set(nums1))]

class Solution:
    def closeString(self, word1, word2):
        if len(word1) != len(word2):
            return False

        c1, c2 = Counter(word1), Counter(word2)
        return (c1.keys() == c2.keys() and sorted(c1.values()) == sorted(c2.values)) 

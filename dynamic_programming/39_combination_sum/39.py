class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        dp = [[] for _ in range(target + 1))]

        for cand in candidates:
            for i in range(cand, target + 1):
                if i == cand:
                    dp[i].append([cand])
                for elem in dp[i-cand]:
                    dp[i].append(elem + [cand])

        return dp[-1]

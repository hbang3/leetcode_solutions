class Solution:
    def permute(self, nums: List[int]):
        ret = []
        def helper(acc, seen):
            if len(seen) == len(nums):
                ret.append(acc)

            for n in nums:
                if n in seen:
                    continue
                seen.add(n)
                helper(acc + [n], seen)
                seen.remove(n)

        helper([], set())
        return ret

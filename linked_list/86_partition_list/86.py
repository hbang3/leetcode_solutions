# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def partition(self, head: Optional[ListNode], x: int) -> Optional[ListNode]:
        sml_list = ListNode()
        sml_head = sml_list

        lrg_list = ListNode()
        lrg_head = lrg_list

        while head:
            if head.val < x:
                sml_list.next = ListNode(val = head.val)
                sml_list = sml_list.next
            else:
                lrg_list.next = ListNode(val = head.val)
                lrg_list = lrg_list.next
            head = head.next

        sml_list.next = lrg_head.next

        return sml_head.next

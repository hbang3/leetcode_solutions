### 86
Given the head of a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

Input: head = [1,4,3,2,5,2], x = 3
Output: [1,2,2,4,3,5]

Input: head = [2,1], x = 2
Output: [1,2]

#### Intuition
make two linked list, one stores smaller value then x and the other stores larger value then x.
iterate main linked list and populate sml_list and lrg_list.
Join two linked list

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def deleteDuplicates(self, head: Optional[ListNode]) -> Optional[ListNode]:
        dummy1 = ListNode(val=-102)
        dummy2 = ListNode(val=-101)
        dummy1.next = dummy2
        dummy2.next = head

        prev = dummy1
        slow = dummy2
        fast = head
        while fast:
            if slow.val != fast.val:
                prev = prev.next
                slow = slow.next
                fast = fast.next
            else:
                while fast and slow.val == fast.val:
                    fast = fast.next
                prev.next = fast
                slow = fast
                if fast:
                    fast = fast.next
         
        return dummy2.next

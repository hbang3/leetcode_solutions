### 82
Given the head of a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list. Return the linked list sorted as well.

Input: head = [1,2,3,3,4,4,5]
Output: [1,2,5]


Input: head = [1,2,3,3,4,4,5]
Output: [1,2,5]


#### Intuition
three pointer
dummy1 -> dummy 2 -> head

prev      slow       fast

loop:
    if slow != fast then (* no dup *)
        prev = prev.next
        slow = slow.next
        fast = fast.next
    else:
        move fast until slow.val != fast.val:
        prev.next = fast
        slow = fast
        fast = fast.next (if fast.next is not None)

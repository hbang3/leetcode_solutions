### 61 Rotate List
Given the head of a linked list, rotate the list to the right by k places.

Input: head = [1,2,3,4,5], k = 2
Output: [4,5,1,2,3]

Input: head = [0,1,2], k = 4
Output: [2,0,1]

#### Intuition
1. find the length of the linked list and end node of the linked list
2. end.next = head and make circular linked list
(2.5) k = k % length
3. slow, fast = head, head
4. do fast = fast.next k times
5. move fast node  and slow node while fast node reaches the end node
6. ret = slow.next
7. slow.next = None (Make it non circular again)
8. return ret``

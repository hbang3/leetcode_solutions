# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if k == 0 or not head:
            return head

        end = head
        length = 1
        while end.next:
            length += 1
            end = end.next
        # make this circular linked list
        k = k % length
        end.next = head

        slow = head
        fast = head
        for _ in range(k):
            fast = fast.next

        while fast != end:
            slow = slow.next
            fast = fast.next
        res = slow.next
        slow.next = None

        return res

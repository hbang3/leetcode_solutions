class Solution:
    def oddEvenList(self, head):
        if head is None:
            return head

        odd_h = head
        even_h = head.next
        cnt = 1
        p = head
        while p.next:
            next_node = p.next
            if p.next.next is None:
                if cnt % 2 == 1:
                    p.next = even_p
                else:
                    p.next.next = even_p
                    p.next = None
                return head

            p.next = p.next.next
            p = next_node
            cnt += 1
        
        # this is necessary in case head.next is None
        return head
